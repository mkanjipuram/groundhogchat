import { Component, OnInit } from "@angular/core";
import { NgxIndexedDBService, IndexDetails } from "ngx-indexed-db";
import { ActivatedRoute, Router } from "@angular/router";
import { interval, Observable, Subscription } from "rxjs";

@Component({
  selector: "app-chat-board",
  templateUrl: "./chat-board.component.html",
  styleUrls: ["./chat-board.component.scss"],
})
export class ChatBoardComponent implements OnInit {
  id: string = "1";
  mySub: Subscription;
  constructor(
    private dbService: NgxIndexedDBService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.mySub = interval(2000).subscribe((func) => {
      this.getMessage();
      this.getAllActiveAndInactiveUsers();
    });
    this.getAllActiveAndInactiveUsers();
    
  }

  ngOnInit() {

  }

  allUsers: any;
  tempactiveUsers = [];
  activeUsers = [];
  inactiveUsers = [];
  tempinactiveUsers =[];
  currentUser = [];
  userName: string;
  status: string;
  senderid: string;
  indexid: any;
  indexEmail: any;
  password: any;
  getAllActiveAndInactiveUsers() {
    this.allUsers=[];
    this.tempactiveUsers = [];
    this.tempinactiveUsers =[];
    console.log("Called this");
    this.dbService.getAll("userTable").subscribe((userTable) => {
      this.allUsers = userTable;
      console.log(this.allUsers);

      if (this.allUsers.length >= 1) {
        this.allUsers.forEach((element) => {
          if (
            element.status === "Active" &&
            element.uuid != this.route.snapshot.params["id"]
          ) {
            this.tempactiveUsers.push(element);
          } else if (
            element.status === "Inactive" &&
            element.uuid != this.route.snapshot.params["id"]
          ) {
            this.tempinactiveUsers.push(element);
          } else if(
            element.uuid === this.route.snapshot.params["id"] 
           ){
            this.userName = element.full_name;
            this.status = element.status;
            this.senderid = element.uuid;
            this.indexid = element.id;
            this.indexEmail = element.email;
            this.password = element.password;
            console.log(this.userName);
          }
        });
        this.activeUsers = this.tempactiveUsers;
        this.inactiveUsers = this.tempinactiveUsers;
      }
    });
  }

  recipientID: any;
  showChatBox: Boolean;
  recipientName: string;
  recipientEmail: string;
  allRecipients = [];
  currentRecipient = [];
  onChatSelect(i) {
    //1. Get the senderID (current user id) - Already available from the above function.
    //1.1 Clear the recipientID if any.
    this.recipientID = "";
    //2. Get the recipientID (form.index.id, whosever chat is selected)
    this.recipientID = i.uuid;
    //3. Change showChatBox Boolean to true to show the chat box
    this.showChatBox = true;
    console.log(i.uuid);
    //4. Get all the recipient details for the session.
    this.dbService.getAll("userTable").subscribe((userTable) => {
      this.allRecipients = userTable;
      if (this.allRecipients.length > 1) {
        this.allRecipients.forEach((element) => {
          if (element.uuid === this.recipientID) {
            (this.recipientName = element.full_name),
              (this.recipientEmail = element.email);
          }
          this.getMessage();
        });
      }
    });
  }

    //1.Check if senderid is equal to current id.
    //2. Check if recipientID is available
    //3. Generate a time stamp
    //4. Query the transaction table with these two IDs, and check the last sequence number.
    //5. If not available, start from 1. If available then increment it.
  timeStamp: Date;
  sequenceNo: Number;
  tokenizedChat = [];
  postMessage(form) {
    console.log("HITTING");
    const data = form.value;
    this.usersChat = [];
    this.tokenizedChat = [];
      this.dbService
        .getAll("transactionTable")
        .subscribe((transactionTable) => {
          this.allChat = transactionTable;
          if (this.allChat.length >= 0) {
            this.allChat.forEach((element) => {
              if (
                (element.senderid === this.senderid && element.recipientid === this.recipientID) ||
                (element.senderid === this.recipientID && element.recipientid === this.senderid)
              ) {
                this.usersChat.push(element);
              }
            });
          
            if(this.usersChat[this.usersChat.length -1 ] != undefined){
             
              this.tokenizedChat = this.usersChat.sort((a,b) => a['token'] - b['token']);
              let num = this.tokenizedChat[this.tokenizedChat.length -1]
              this.sequenceNo = ++num.token
            } else {
              this.sequenceNo = 0;
            }
            console.log(this.sequenceNo);
            this.timeStamp = new Date();
            console.log(this.timeStamp);
            console.log(this.usersChat);
            this.dbService
              .add("transactionTable", {
                senderid: this.senderid,
                senderName: this.userName,
                recipientid: this.recipientID,
                message: data.message,
                token: this.sequenceNo,
                time: this.timeStamp,
              })
              .subscribe((key) => {
                console.log("key: ", key);
                this.getMessage();
                form.reset();
              });
          }
        });
    
  }
  allChat = [];
  usersChat = [];
  organizedChat = [];
  getMessage() {
    this.allChat = [];
    this.usersChat = [];
    //1.check if transaction table is available.
    //2. organize the array in an order based on the token number.
    this.dbService.getAll("transactionTable").subscribe((transactionTable) => {
      this.allChat = transactionTable;
      if (this.allChat.length >= 0) {
        this.allChat.forEach((element) => {
          if (  (element.senderid === this.senderid && element.recipientid === this.recipientID) ||
          (element.senderid === this.recipientID && element.recipientid === this.senderid)) {
            this.usersChat.push(element);
          }
        });
      }
      this.organizedChat = this.usersChat.sort((a,b) => a['token'] - b['token']);
    });
  }

  onLogout(){
    this.dbService.update("userTable", {
      id: this.indexid,
      full_name: this.userName,
      email: this.indexEmail,
      password: this.password,
      uuid: this.senderid,
      status: "Inactive",
    }).subscribe((data) => {
      console.log(data);
      this.router.navigate(["/"]);    
    });
  }
  ngOnDestroy() {
    this.mySub.unsubscribe();
  }
}
