import { Component, OnInit } from "@angular/core";
import { NgxIndexedDBService, IndexDetails } from "ngx-indexed-db";
import { Guid } from "guid-typescript";
import { Router } from "@angular/router";
//import { MatStepperModule } from "@angular/material/stepper";
//import { UniqueSelectionDispatcher } from "@angular/cdk/collections";
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: "app-authentication",
  templateUrl: "./authentication.component.html",
  styleUrls: ["./authentication.component.scss"],
})
export class AuthenticationComponent implements OnInit {
  title = "groundhog-chat";
  returnMsg: string;
  submitted: boolean = false;
  fileData: any;

  constructor(
    private dbService: NgxIndexedDBService,
    private router: Router,
    //private stepper: MatStepperModule,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {}

  userid: any;
  x: any;
  submitRegistrationData(form) {
    //get value of form
    const data = form.value;
    this.submitted = true;
    //check if form is valid
    if (data.full_name && data.email && data.password) {
      //create a guid, we shall use this id in the URL param to differentiate between the users.
      this.x = Guid.create();
      //add data to indexedDB
      this.dbService
        .add("userTable", {
          full_name: data.full_name,
          email: data.email,
          password: data.password,
          status: "Active",
          uuid: this.x.value,
        })
        .subscribe((key) => {
          this.returnMsg = "Data added successfully";
          console.log("key: ", key);
          this.userid = key;
          console.log(this.userid);
          this.router.navigate(["/chatBoard/" + this.x.value]);
          this.openSnackBar("Registered and Logged In", "SUCCESS")
        });
      form.reset();
    } else {
      //return an error
      this.openSnackBar("Something Went Wrong", "ERROR")
    }
  }

  allUsers: any;
  loggedinUser: any;
  submitLoginData(form) {
    //get value of form
    const data = form.value;

    this.dbService.getAll("userTable").subscribe((userTable) => {
      this.allUsers = userTable;
      console.log(this.allUsers.length);
      if (this.allUsers.length >= 0) {
        this.allUsers.forEach((element) => {
          if (element.email === data.email) {
            if (element.password === data.password) {
              this.loggedinUser = element;
              this.dbService.update("userTable", {
                id: this.loggedinUser.id,
                full_name: this.loggedinUser.full_name,
                email:  this.loggedinUser.email,
                password: this.loggedinUser.password,
                uuid: this.loggedinUser.uuid,
                status: "Active",
              }).subscribe((data) => {
                form.reset();
                this.router.navigate(["/chatBoard/" + element.uuid]);  
                this.openSnackBar("Succesful Login", "SUCCESS");
              });
            } else {
              console.log("Password is wrong");
              this.openSnackBar("Password Match Failed", "ERROR")
              form.reset();
            }
          } else {
            this.openSnackBar("Email ID or Password is Wrong", "ERROR");
            form.reset();
          }
        });
      } else {
        this.openSnackBar("No Users Found", "ERROR")
        form.reset();
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }
}
