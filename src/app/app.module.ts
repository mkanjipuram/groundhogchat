import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Importing Indexed-db and initializing it. 
//Could use reactive or template driven forms. Using template, so Formsmodule
import { DBConfig, NgxIndexedDBModule } from 'ngx-indexed-db';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Comps
import { AuthenticationComponent } from './authentication/authentication.component';
import { ChatBoardComponent } from './authentication/chat-board/chat-board.component';

//Material Modules
import {MatStepperModule} from '@angular/material/stepper';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';


//Defining Schema
//Naming the Table
//Configuring them in the browserDB

const dbConfig: DBConfig = {
  name: 'groundHogChat',
  version: 1,
  objectStoresMeta: [{
    store: 'userTable',
    storeConfig: {keyPath: 'id', autoIncrement: true},
    storeSchema: [
      {name: 'full_name', keypath: 'full_name', options: { unique: false}},
      {name: 'email', keypath: 'email', options: { unique: true}},
      {name: 'password', keypath: 'password', options: { unique: false}},
      {name: 'status', keypath: 'status', options: { unique: false}},
      {name: 'uuid', keypath: 'uuid', options: { unique: true}},
    ] 
  },
  {store: 'transactionTable',
  storeConfig: {keyPath: 'id', autoIncrement: true},
  storeSchema: [
    {name: 'senderid', keypath: 'senderid', options: { unique: false}},
    {name: 'senderName', keypath: 'senderName', options: {unique: false}},
    {name: 'recipientid', keypath: 'recipientid', options: { unique: false}},
    {name: 'message', keypath: 'message', options: { unique: false}},
    {name: 'token', keypath: 'token', options: {unique: false},},
    {name: 'time', keypath: 'message', options: {unique: false}}
  ]
  }
]
  
};


@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    ChatBoardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxIndexedDBModule.forRoot(dbConfig),
    FormsModule,
    BrowserAnimationsModule, 
    MatStepperModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    MatIconModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
