import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './authentication/authentication.component';
import { ChatBoardComponent } from './authentication/chat-board/chat-board.component';

export const routes: Routes = [
{ path: '', redirectTo: '/authentication', pathMatch: 'full' },
{ path: 'authentication', component: AuthenticationComponent},
{ path: 'chatBoard/:id', component: ChatBoardComponent},
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
  })

  export class AppRoutingModule {}