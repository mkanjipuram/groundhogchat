import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'groundhog-chat';
  returnMsg: string;
  submitted: boolean = false;
  fileData: any;

  constructor(){
  }
}

